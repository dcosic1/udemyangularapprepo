import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Recipe } from '../recipes.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  recipes: Recipe[] = [
    new Recipe('Pasta', 'This is description about pasta recipe', 'https://img1.cookinglight.timeinc.net/sites/default/files/styles/4_3_horizontal_-_1200x900/public/image/2017/05/main/pasta-chickpea-sauce-1707p46.jpg?itok=Jmv3zJMU'),
    new Recipe('Pizza', 'This is description about pizza recipe', 'https://food-images.files.bbci.co.uk/food/recipes/alpine_pizza_32132_16x9.jpg')
  ];
  
  // Slanje podataka prema komponenti recipes
  @Output() recipeFromListSelected = new EventEmitter<Recipe>();

  constructor() { }

  ngOnInit() {
  }

  onRecipeSelected(recipe: Recipe){
    this.recipeFromListSelected.emit(recipe);
  }

}
