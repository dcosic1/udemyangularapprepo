import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Recipe } from '../../recipes.model';

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.css']
})
export class RecipeItemComponent implements OnInit {
  
  // when getting recipe from the outside we are using decorator @Input ('aliasWouldGoHere');

  @Input() recipe: Recipe;
  // @Output() direktiva se koristi da se posalje event u recipe-list metoda: onSelected(), gdje se dalje salje u recipe komponentu
  @Output() recipeSelected = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

  onSelected(){
    this.recipeSelected.emit();
  }

}
