export class Recipe{
    // This is a model for recipe
    // Define how Recipe should look like

    public name: string;
    public description: string;
    // Image will be showed using urls 
    public imagePath: string;

    constructor(name: string, description:string, imagePath:string) {
        this.name = name;
        this.description = description;
        this.imagePath = imagePath;
    }

}